import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;

public class Entity {
    @Retention(RetentionPolicy.RUNTIME)
    @interface Secured {
        String value() default "strict";

        int age();
    }
    public static void main(String[] args) {
        Entity entity = new Entity();
        Class c = entity.getClass();
        Method[] methods = c.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println("Имя: " + method.getName());
            System.out.println("Возвращаемый тип: " + method.getReturnType().getName());

            Class[] paramTypes = method.getParameterTypes();
            System.out.print("Типы параметров: ");
            for (Class paramType : paramTypes) {
                System.out.print(" " + paramType.getName());
            }
            System.out.println();
        }
        System.out.println("Ann=" );
        entity.Print1();
    }

    @Secured(age = 11)
    public void Print1() {
        Entity entity = new Entity();

        try {
            Class c = entity.getClass();

            Method m = c.getMethod("Print2");

            Secured anno = m.getAnnotation(Secured.class);


            System.out.println(anno.value() + " " + anno.age());
        } catch (NoSuchMethodException exc) {
            System.out.println("Method Not Found.");
        }
    }



    @Secured(age = 1, value = "string")
    public void Print2() {
        System.out.println("2");
    }

    private void Print3() {
        System.out.println("3");
    }

    private void Print4() {
        System.out.println("4");
    }
}

